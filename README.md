# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* TCP/IP bandwidh monitoring with PHP agent, InfluxDB and Grafana
* v0.0.4
### How do I get set up? ###

* To set up, clone the repository, enter the repository directory and run `composer install` to fetch the dependencies.
* Configuration is located at `config.php` file. There you could define database address url.
* Dependencies:
    * [InfluxDB](https://influxdata.com/)
    * [Grafana](http://grafana.org/)
    * [PHP >= 5.4.0](http://php.net/)
* To deploy the app, install InfluxDB, Grafana and setup the application. Create Grafana and InfluxDB users with InfluxDB database and run
```shell
sudo php index.php
```
Root privileges are required to connect to TCP socket.

### Contribution guidelines ###

* Use [PHPUnit](https://phpunit.de/) for testing
* Use [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) and [PHPMD](https://phpmd.org/) for code review
* Use [PSR-4](http://www.php-fig.org/psr/psr-4/) for autoloading

### Who do I talk to? ###

* Admin: [Povilas B.](https://lt.linkedin.com/in/povilasbrilius)
* Other team member contact: [Medardas M.](https://www.linkedin.com/in/medardas-mikolajunas-b9939986)